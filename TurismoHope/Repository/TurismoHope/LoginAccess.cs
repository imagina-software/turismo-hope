﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using TurismoHope.Models;

namespace TurismoHope.Repository.TurismoHope
{
    public class LoginAccess
    {

        public IEnumerable<Usuario> Login(string pUserName, string pPassword)
        {

            string lineagg = "0";

            try
            {

                List<Usuario> lstUserProfile = new List<Usuario>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.HIT_LeeUsuarioPassword_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@UserName", SqlDbType.VarChar).Value = pUserName;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar).Value = pPassword;

                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",4";
                        if (rdr["UserName"].ToString().Trim() == pUserName.Trim())
                        {

                            Usuario fuserprofile = new Usuario
                            {
                                UserId = Convert.ToInt32(rdr["UserId"]),
                                UserName = rdr["UserName"].ToString(),
                                //IsActive = Convert.ToInt32(rdr["IsActive"]),
                                Nombre = rdr["Nombre"].ToString(),
                                ApPaterno = rdr["ApPaterno"].ToString(),
                                ApMaterno = rdr["ApMaterno"].ToString(),
                                Email = rdr["Email"].ToString()
                            };

                            lstUserProfile.Add(item: fuserprofile);

                        }

                    }
                    lineagg += ",5";
                    con.Close();
                }

                return lstUserProfile;

            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }

    }
}