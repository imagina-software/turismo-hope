﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using TurismoHope.Models;

namespace TurismoHope.Repository.TurismoHope
{
    public class MantenimientoAccess
    {
        #region Registro de Usuario
        public void RegistrarUsuario(Int16 pNroUsuario, string pUserName, string pPassword, string pNombre, string pApPaterno, string pApMaterno, string pEmail)
        {
            string codigo = "";

            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.HIT_RegistrarUsuario_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pNroUsuario", SqlDbType.Int).Value = pNroUsuario;
                    cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = pUserName;
                    cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = pPassword;
                    cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = pNombre;
                    cmd.Parameters.Add("@pApPaterno", SqlDbType.VarChar).Value = pApPaterno;
                    cmd.Parameters.Add("@pApMaterno", SqlDbType.VarChar).Value = pApMaterno;
                    cmd.Parameters.Add("@pEmail", SqlDbType.VarChar).Value = pEmail;


                    con.Open();
                    codigo = (String)cmd.ExecuteScalar();

                    con.Close();
                }


            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }


        public void RegistrarUsuario2(Usuario objUser)
        {
            string codigo = "";
            //char activo;

            //if (objUser.IsActive)
            //{
            //    activo = 'S';
            //}
            //else
            //{
            //    activo = 'N';
            //}



            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.HIT_RegistrarUsuario_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pNroUsuario", SqlDbType.Int).Value = objUser.UserId;
                    cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = objUser.UserName;
                    cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = objUser.Password;
                    cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objUser.Nombre;
                    cmd.Parameters.Add("@pApPaterno", SqlDbType.VarChar).Value = objUser.ApPaterno;
                    cmd.Parameters.Add("@pApMaterno", SqlDbType.VarChar).Value = objUser.ApMaterno;
                    cmd.Parameters.Add("@pEmail", SqlDbType.VarChar).Value = objUser.Email;
                    cmd.Parameters.Add("@pActivo", SqlDbType.Char).Value = objUser.IsActive;





                    con.Open();
                    codigo = (String)cmd.ExecuteScalar();

                    con.Close();
                }


            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }

        public void ActualizarUsuario(Usuario objUser)
        {
            string codigo = "";

            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.HIT_ActualizarUsuario_U", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pNroUsuario", SqlDbType.Int).Value = objUser.UserId;
                    cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = objUser.UserName;
                    cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objUser.Nombre;
                    cmd.Parameters.Add("@pApPaterno", SqlDbType.VarChar).Value = objUser.ApPaterno;
                    cmd.Parameters.Add("@pApMaterno", SqlDbType.VarChar).Value = objUser.ApMaterno;
                    cmd.Parameters.Add("@pEmail", SqlDbType.VarChar).Value = objUser.Email;
                    cmd.Parameters.Add("@pActivo", SqlDbType.Bit).Value = objUser.IsActive;


                    con.Open();
                    codigo = (String)cmd.ExecuteScalar();

                    con.Close();
                }


            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }

        public void EliminarUsuario(int pUserId)
        {

            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.HIT_EliminarUsuario_D", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", pUserId);

                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

            }
            catch (Exception ex)
            {

                throw;
            }


        }


        public void ActualizarContraseña(int pUserId, string pPassword)
        {

            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.HIT_ActualizarContraseña_U", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pUserId", pUserId);
                    cmd.Parameters.AddWithValue("@pContraseña", pPassword);


                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }

            }
            catch (Exception ex)
            {

                throw;
            }


        }

        public List<Usuario> ListarUsuario(int pUserId)
        {
            var lista = new List<Usuario>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.HIT_ListarUsuario_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@UserId", pUserId);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Usuario
                    {
                        UserId = x.Field<int>("UserId"),
                        UserName = x.Field<string>("UserName"),
                        Nombre = x.Field<string>("Nombre"),
                        ApPaterno = x.Field<string>("ApPaterno"),
                        ApMaterno = x.Field<string>("ApMaterno"),
                        Email = x.Field<string>("Email"),
                        IsActive = x.Field<bool>("IsActive")
                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Usuario>();
            }

            return lista;
        }
        #endregion



    }
}