﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TurismoHope.Models
{
    public class Login
    {
        [Required(ErrorMessage = "Por favor ingrese un nombre de usuario")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "Por favor ingrese un password")]
        public string Password { get; set; }
    }
}