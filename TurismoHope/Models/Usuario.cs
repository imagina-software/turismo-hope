﻿using System.ComponentModel.DataAnnotations;

namespace TurismoHope.Models
{
    public class Usuario
    {
        public int UserId { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un nombre de usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Por favor seleccione un usuario")]
        public string UserNameUpdate { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un password")]
        public string PasswordUpdate { get; set; }

        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Por favor ingrese el nombre")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Por favor ingrese apellido paterno")]
        public string ApPaterno { get; set; }

        [Required(ErrorMessage = "Por favor ingrese apellido materno")]
        public string ApMaterno { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un Email")]
        public string Email { get; set; }

    }
}