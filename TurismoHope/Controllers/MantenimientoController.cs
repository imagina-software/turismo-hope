﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using TurismoHope.Models;
using TurismoHope.Repository.TurismoHope;


namespace TurismoHope.Controllers
{
    public class MantenimientoController : Controller
    {
        // GET: Mantenimiento
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult MantenerUsuario()
        {

            return View();
        }

        [HttpPost]
        public JsonResult MantenerUsuario(Usuario objUser)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";


            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();


                    if (objUser.UserId.Equals(0))
                    {

                        accion = "N";
                    }
                    else
                    {
                        accion = "M";

                    }


                    switch (accion)
                    {
                        case "N":
                            objMantenimiento.RegistrarUsuario2(objUser);
                            mensaje = "Se grabo correctamente la información.";
                            break;

                        case "M":
                            objMantenimiento.ActualizarUsuario(objUser);
                            mensaje = "Se actualizo correctamente la información.";
                            break;

                        case "E":
                            objMantenimiento.EliminarUsuario(objUser.UserId);
                            mensaje = "Se elimino correctamente la información.";
                            break;
                    }

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";


                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });
        }

        public JsonResult EliminarUsuario(Int16 pNumUsuario = 0)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";


            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();




                    objMantenimiento.EliminarUsuario(pNumUsuario);
                    mensaje = "Se elimino correctamente la información.";

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";


                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });


        }
        [HttpPost]

        public ActionResult ListarUsuario2(Int16 pNumUsuario = 0)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";
            List<Usuario> lista;



            try
            {
                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarUsuario(pNumUsuario);

                if (pNumUsuario == 0)
                {
                    mensaje = _serializer.Serialize(lista.ToList());
                }
                else
                {


                    mensaje = _serializer.Serialize(lista.Where(col => col.UserId == pNumUsuario).ToList());
                }

                resultado = "OK";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";
            }

            return Json(new { data = mensaje, indicador = resultado }, JsonRequestBehavior.AllowGet);


        }


        public ActionResult ListaUsuario3(Int16 pNumUsuario = 0)
        {
            List<Usuario> lista;
            MantenimientoAccess objMantenimiento = new MantenimientoAccess();

            lista = objMantenimiento.ListarUsuario(pNumUsuario);

            return Json(new { data = lista }, JsonRequestBehavior.AllowGet);


        }

        [HttpPost]
        public JsonResult ListarUsuario(Int16 pNumUsuario = 0)
        {
            JavaScriptSerializer _serializer = new JavaScriptSerializer();
            String mensaje = "";
            String resultado = "";
            List<Usuario> lista;


            try
            {
                MantenimientoAccess objMantenimiento = new MantenimientoAccess();

                lista = objMantenimiento.ListarUsuario(pNumUsuario);

                if (pNumUsuario == 0)
                {
                    string tabla = "<table class='table datatable-responsive'>";


                    tabla += @"<thead>
                            <tr>
                                <th>Usuario</th>
                                <th>Nombre</th>
                                <th>Apellido Paterno</th>
                                <th>Apellido Materno</th>
                                <th>Email</th>
                                <th>Estado</th>
                                <th>Acciones</th>
                                <th></th>

                            </tr>
                        </thead>
                        <tbody >";




                    foreach (Usuario item in lista)
                    {
                        tabla += String.Format(@"<tr>
                                                <td>{0}</td>
                                                <td>{1}</td>
                                                <td>{2}</td>
                                                <td>{3}</td>
                                                <td>{4}</td>
                                                <td>{5}</td>
                                                <td class='text-center'>
                                                    <div class='list-icons'>
                                                        <div class='dropdown'>
                                                            <a href='#' class='list-icons-item' data-toggle='dropdown'>
                                                                <i class='icon-menu9'></i>
                                                            </a>
                                                            <div class='dropdown-menu dropdown-menu-right'>
                                                                    <a onclick='editarPasajero({6})' class='dropdown-item'><i class='icon-file-pdf'></i>Editar</a>
                                                                    <a onclick='editarContrasena({7})' class='dropdown-item'><i class='icon-file-pdf'></i>Cambiar Constraseña</a>
                                                                    <a onclick='eliminarPasajero({8})' class='dropdown-item'><i class='icon-file-pdf'></i>Eliminar</a>     
                                                            </div>        
                                                        </div>
                                                    </div>

                                                </td>
                                        
                                             </tr>",
                                               item.UserName, item.Nombre, item.ApPaterno, item.ApMaterno, item.Email, item.IsActive == true ? "Activo" : "Inactivo", item.UserId.ToString(), item.UserId.ToString(), item.UserId.ToString());
                    }

                    tabla += "</tbody></table>";



                    mensaje = tabla;
                }
                else
                {
                    mensaje = _serializer.Serialize(lista.Where(col => col.UserId == pNumUsuario).ToList());
                }

                resultado = "OK";
            }
            catch (Exception ex)
            {
                mensaje = ex.Message;
                resultado = "ER";
            }

            return Json(new { contenido = mensaje, indicador = resultado });
        }


        [HttpPost]
        public JsonResult ActualizarConstraseña(Int16 pNumUsuario = 0, string pPassword = "")
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";


            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    MantenimientoAccess objMantenimiento = new MantenimientoAccess();




                    objMantenimiento.ActualizarContraseña(pNumUsuario, pPassword);
                    mensaje = "Se actualizo la constraseña.";

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";


                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });




        }







    }



}