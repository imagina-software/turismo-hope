﻿using System.Linq;
using System.Web.Mvc;
using TurismoHope.Repository.TurismoHope;
using TurismoHope.Models; 

namespace TurismoHope.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Login objUser)
        {

            LoginAccess objLogin = new LoginAccess();

            if (ModelState.IsValid)
            {

                var obj = objLogin.Login(objUser.UserName, objUser.Password);

                //var obj = db.UserProfiles.Where(a => a.UserName.Equals(objUser.UserId) && a.Password.Equals(objUser.Password)).FirstOrDefault();
                if (obj.Count() > 0)
                {

                    Session["UserID"] = obj.FirstOrDefault().UserId;
                    Session["UserName"] = obj.FirstOrDefault().UserName;
                    Session["Nombre"] = obj.FirstOrDefault().Nombre;

                    return RedirectToAction("Index");
                }
                else
                {

                    ViewBag.error = "Usuario o password invalido";

                }


            }
            return View(objUser);
        }

        public ActionResult UserDashBoard()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Remove("UserID");
            return RedirectToAction("Index");
        }
    }
}