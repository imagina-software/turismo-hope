﻿
$(function () {
    debugger

    limpiar();
    listarUsuario(0);
    //listarUsuario2(0);

    $("#btnCancelar").click(function () {
        debugger
        limpiar();
    });

    $("#btnEnviar").click(function () {
        debugger


        if (!$("#RegistrarUsuarioForm").valid()) {

            return false;
        }


        var pregunta = confirm("¿Desea grabar la información ingresada?");

        if (pregunta)
            //registrarUsuario(parametros);
            registrarUsuario();


    });

    $("#btnEnviarContraseña").click(function () {
        debugger


        if (!$("#ActualizarConstraseñaForm").valid()) {

            return false;
        }


        var parametros = {};

        parametros["pNumUsuario"] = $("#txtNroUsuario").val();
        parametros["pPassword"] = $("#txtPassword2").val();



        var pregunta = confirm("¿Desea actualizar la contraseña?");

        if (pregunta)
            //registrarUsuario(parametros);
            actualizarContraseña(parametros);


    });

    $("#btnCancelar").click(function () {
        limpiar();
    });

});

function limpiar() {
    $("#txtUsuario").val("");
    $("#txtPassword").val("");
    $("#txtNombre").val("");
    $("#txtApPaterno").val("");
    $("#txtApMaterno").val("");
    $("#txtEmail").val("");
    $('#chckActivo').prop('checked', false);
    $(".btnAccion").prop("disabled", false);

}

function registrarUsuario() {

    $.ajax({
        type: "POST",
        url: "/Mantenimiento/MantenerUsuario",
        data: $("#RegistrarUsuarioForm").serialize(),
        //data: JSON.stringify(parametros),
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        dataType: "json",
        success: function (response) {
            debugger
            var rpta = response.indicador;
            var mensaje = response.contenido;

            if (rpta == "OK") {
                debugger
                limpiar();
                listarUsuario(0);
                alert(mensaje);


            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}


function eliminarUsuario(parametros) {

    $.ajax({
        type: "POST",
        url: "/Mantenimiento/EliminarUsuario",
        data: JSON.stringify(parametros),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            debugger
            var rpta = response.indicador;
            var mensaje = response.contenido;

            if (rpta == "OK") {
                debugger
                limpiar();
                listarUsuario(0);
                //alert(mensaje);
            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}


//function listarUsuario2(numero) {
//    $(document).ready(function () {
        
//        $(".datatable-responsive").DataTable(
            
//            {
//                "ajax": {
//                    "url": "/Mantenimiento/ListarUsuario2",
//                    "type": "GET",
//                    "datatype": "json"
//                },
//                "columns": [
//                    { "data": "UserId" },
//                    { "data": "UserName" },
//                    { "data": "UserNameUpdate" },
//                    { "data": "Password" },
//                    { "data": "PasswordUpdate" },
//                    { "data": "Nombre" },
//                    { "data": "ApPaterno" },
//                    { "data": "ApMaterno" },
//                    { "data": "Email" }
//                ]

//            });

//    });

//}



function listarUsuario(numero, flag) {


    var parametros = {};
    //parametros["pNroPedido"] = $("#txtNroPedido").val();
    parametros["pNumUsuario"] = numero;
    //parametros["pIdioma"] = $("#txtIdioma").val();

    $.ajax({
        type: "POST",
        url: "/Mantenimiento/ListarUsuario2",
        data: JSON.stringify(parametros),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            debugger
            var rpta = response.indicador;
            var mensaje = response.data;
            console.log(response.data);

            var result = jQuery.parseJSON(response.data);
            console.log(result);
            console.log(result.data);

            if (rpta == "OK") {

                if (numero == 0) {
                    //$("#tablaUsuarios").html(mensaje);
                    $('#table-users').DataTable({
                        "destroy": true,
                        "data": result,
                        //"ajax": {
                        //    "url": result,
                        //    "dataSrc":""
                        //},
                        "columns": [
                            { "data": "UserName"},
                            { "data": "Nombre" },
                            { "data": "ApPaterno" },
                            { "data": "ApMaterno" },
                            { "data": "Email" },
                            {
                                "className:": "text-center",
                                "render": function (data, type, full, meta) {
                                    //return '<a class="btn btn-info" href="/Demo/Edit/' + full.CustomerID + '">Edit</a>';

                                    return '<div class="list-icons"><div class="dropdown"><a href="#" class="list-icons-item" data-toggle="dropdown"><i class="icon-menu9"></i></a><div class="dropdown-menu dropdown-menu-right">' +
                                        '<a onclick="editarPasajero(' + full.UserId +')" class="dropdown-item"><i class="icon-pencil7 mr-3 icon-1x"></i> Editar</a>' +
                                        '<a onclick="editarContrasena(' + full.UserId +')" class="dropdown-item"><i class="icon-file-locked mr-3 icon-1x"></i> Cambiar contraseña</a>' +
                                        '<a onclick="eliminarPasajero(' + full.UserId +')" class="dropdown-item"><i class="icon-trash-alt mr-3 icon-1x"></i> Eliminar</a>' +
                                        '</div></div></div>';
                                }
                            },

                            //{
                            //    data: null, render: function (data, type, row) {
                            //        return "<a href='#' class='btn btn-danger' onclick=DeleteData('" + row.CustomerID + "'); >Delete</a>";
                            //    }
                            //},



                        ]
                        
                    });

                } else {
                    var datos = $.parseJSON(mensaje);

                    if (datos.length > 0) {
                        var fila = datos[0];
                        //var fecha = formatDate(fila.FormatFchNacimiento);

                        if (flag == "EP") {

                            $("#txtUsuario").val(fila.UserName);
                            $("#txtNombre").val(fila.Nombre);
                            $("#txtApPaterno").val(fila.ApPaterno);
                            $("#txtApMaterno").val(fila.ApMaterno);
                            $("#txtEmail").val(fila.Email);

                            if (fila.IsActive == true) {
                                $('.uniform-checker span').prop('checked', true);
                            }

                  
                        } else {
                            $("#txtUsuario2").val(fila.UserName);

                        }

                        //$("#txtUsuario").val(fila.UserName);
                        //$("#txtNombre").val(fila.Nombre);
                        //$("#txtApPaterno").val(fila.ApPaterno);
                        //$("#txtApMaterno").val(fila.ApMaterno);
                        //$("#txtEmail").val(fila.Email);

                    }

                }
            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });

    //$('#tablaUsuarios').DataTable({
    //    "paging": true,
    //    "lengthChange": false,
    //    "searching": true,
    //    "ordering": true,
    //    "info": true,
    //    "autoWidth": false,
    //});
}

function editarPasajero(numero) {
    debugger
    $("#txtNroUsuario").val(numero);
    $(".btnAccion").prop("disabled", true);
    $("#txtPassword").prop("disabled", true);


    listarUsuario(numero, 'EP');
}

function editarContrasena(numero) {
    debugger
    $("#txtNroUsuario").val(numero);
    $(".btnAccion").prop("disabled", true);
    $(".btnPasajero").prop("disabled", true);
    listarUsuario(numero, 'EC');
}

function eliminarPasajero(numero) {
    debugger
    var pregunta = confirm("¿Desea eliminar el pasajero?");

    //Swal.fire({
    //    title: 'Estas seguro?',
    //    text: "No podras revertir esto!",
    //    icon: 'warning',
    //    showCancelButton: true,
    //    confirmButtonColor: '#3085d6',
    //    cancelButtonColor: '#d33',
    //    confirmButtonText: 'Sí, eliminar!'
    //}).then((result) => {
    //    if (result.value) {

    //        var parametros = {};

    //        parametros["pNumUsuario"] = numero;

    //        eliminarUsuario(parametros);

    //        Swal.fire(
    //            'Eliminado!',
    //            'El usuario ha sido eliminado.',
    //            'success'
    //        )
    //    }
    //})

    if (pregunta) {

        var parametros = {};

        parametros["pNumUsuario"] = numero;
        parametros["pAccion"] = "E";

        eliminarUsuario(parametros);
    }


}

function actualizarContraseña(parametros) {

    $.ajax({
        type: "POST",
        url: "/Mantenimiento/ActualizarConstraseña",
        data: JSON.stringify(parametros),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            debugger
            var rpta = response.indicador;
            var mensaje = response.contenido;

            if (rpta == "OK") {
                debugger
                limpiar();
                listarUsuario(0);
                alert(mensaje);
            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}
//$(function () {
//    $("#tablaUsuarios").DataTable();
//    $('#tablaUsuarios').DataTable({
//        "paging": true,
//        "lengthChange": false,
//        "searching": false,
//        "ordering": true,
//        "info": true,
//        "autoWidth": false,
//    });
//});

